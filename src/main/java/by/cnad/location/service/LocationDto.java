package by.cnad.location.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LocationDto {

  private Long id;
  private String name;
  private String address;
  private String country;
  private String city;
  private Long companyId;
}
